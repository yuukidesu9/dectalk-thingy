A DECTalk thingy I decided to code outta nowhere.

**John Madden?** Yes, John Madden indeed! It's the same TTS used in Moonbase Alpha, and also by Stephen Hawking.

## But how do I use it?

It's simple! You should have [NodeJS and npm](https://nodejs.org/en/download/) installed on your machine. If so, it's just a matter of `npm install` inside the folder where you cloned or extracted the repo's contents.

After you install it, you can just use `node cli.js "your text here"` and paste any DECTalk songs you find (mainly from the Moonbase Alpha Steam Community forums).

Have fun!

*P.S.: It also outputs a .WAV file on the folder, in case you want to post the audio to Discord.*
