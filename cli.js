#!/usr/bin/env node

const { say } = require('dectalk');
const { writeFileSync } = require('fs');
const player = require('node-wav-player');
const [ ,, ...args ] = process.argv;

const string = args.join(' ');
(async () => {
    const WavData = await say(string);
    writeFileSync('./file.wav', WavData);
    player.play({
		path: "./file.wav",
	}).then(() => {
		console.log("Playing back sound.");
	}).catch((error) => {
		console.error(error);
	});
})();
